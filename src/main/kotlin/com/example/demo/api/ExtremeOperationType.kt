package com.example.demo.api

import com.example.demo.services.operationSolver.operation.Operation
import kotlin.reflect.KClass

data class ExtremeOperationType(
        val operationClass: KClass<out Operation>,
        val name: String,
        val placeholder: String
) {
    override fun toString() = name

    fun parseArg(arg: String) = arg.removeSurrounding(
            placeholder.commonPrefixWith(arg),
            placeholder.commonSuffixWith(arg)
    )

    companion object {
        fun getDefault() = ExtremeOperationType(Operation::class, "", "not solved")
    }
}