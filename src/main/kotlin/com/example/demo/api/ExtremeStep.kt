package com.example.demo.api

import com.example.demo.ExtremeCalcApp.Companion.newUniqueAppFile
import com.example.demo.services.historyService.HistoryStep

class ExtremeStep(override var number: Int) : HistoryStep {
    val initOperand = ExtremeFileOperand(newUniqueAppFile())
    val resultOperand = ExtremeFileOperand(newUniqueAppFile())
    lateinit var operationType: ExtremeOperationType
    lateinit var userInput: String

    override fun toString() = "step: $number type: (${operationType.name}) $userInput"

}