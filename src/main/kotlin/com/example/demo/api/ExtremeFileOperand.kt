package com.example.demo.api

import com.example.demo.services.operationSolver.operation.operand.SimpleFileOperand
import com.example.demo.view.dynamicViewer.dataBlockProvider.DataBlockProvider
import java.io.EOFException
import java.io.File
import java.io.RandomAccessFile
import kotlin.math.ceil

class ExtremeFileOperand(
        file: File,
        override val blockSize: Int = DEFAULT_BLOCK_SIZE
) : SimpleFileOperand(file), DataBlockProvider<String> {  //todo: add App-specific abstraction
    override lateinit var onUpdate: () -> Unit
    var writeCount = 0

    override fun append(n: Float) = synchronized(file) {
        super.append(n)
        if (writeCount++ % outBuffer == 0)
            fireChange()
    }

    override fun clear() {
        super.clear()
        writeCount = 0
        fireChange()
    }

    override fun close() {  //todo: add finalize or release or destroy logic instead
        super.close()
        fireChange()
    }

    override fun getBlock(i: Int): String = synchronized(file) { readBlock(i) }

    override fun getBlockCount() = ceil(file.length().toDouble() / Float.SIZE_BYTES / blockSize).toInt()

    fun writeFromFile(newFile: File) {
        newFile.copyTo(file)
        length = newFile.length() / Float.SIZE_BYTES
        fireChange()
    }

    fun isEmpty() = file.length() == 0L

    private fun RandomAccessFile.seekBlock(block: Int) = seek(block.toLong() * blockSize * Float.SIZE_BYTES)

    private fun RandomAccessFile.readStringBlock(): String {
        val sb = StringBuilder()
        var readed = 0
        while (readed < blockSize && filePointer < length()) {
            sb.append("${readFloat()}  ")
            readed++
        }
        return sb.toString()
    }

    private fun fireChange() = onUpdate()

    private fun readBlock(blockNum: Int): String {
        RandomAccessFile(file, "r").use { dataProvider ->
            dataProvider.seekBlock(blockNum)
            return try {
                dataProvider.readStringBlock()
            } catch (ex: EOFException) {
                println("=================== LOX =====================")
                ""
            }
        }
    }

    private fun File.copyTo(target: File) {
        target.writer().use { it.flush() }
        this.inputStream().use { input ->
            target.outputStream().use { output ->
                input.copyTo(output, 1_000_000)
            }
        }
    }

    companion object {
        const val DEFAULT_BLOCK_SIZE = 5
    }
}