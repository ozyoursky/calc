package com.example.demo

import com.example.demo.api.ExtremeFileOperand
import com.example.demo.api.ExtremeOperationType
import com.example.demo.api.ExtremeStep
import com.example.demo.services.fileConverter.FileConverter
import com.example.demo.services.historyService.HistoryService
import com.example.demo.services.operationSolver.OperationSolver
import com.example.demo.services.operationSolver.operation.operations.AverageOperation
import com.example.demo.services.operationSolver.operation.operations.DivideOperation
import com.example.demo.services.operationSolver.operation.operations.FactorialOperation
import com.example.demo.services.operationSolver.operation.operations.LogarithmOperation
import com.example.demo.services.operationSolver.operation.operations.MedianOperation
import com.example.demo.services.operationSolver.operation.operations.MinusOperation
import com.example.demo.services.operationSolver.operation.operations.MultipleOperation
import com.example.demo.services.operationSolver.operation.operations.PlusOperation
import com.example.demo.services.operationSolver.operation.operations.RaiseOperation
import com.example.demo.services.operationSolver.operation.operations.SqrtOperation
import com.example.demo.services.operationSolver.operation.operations.StandardDeviationOperation
import com.example.demo.services.operationSolver.operation.Operation.Companion.HUNDRED_PERCENT
import com.example.demo.services.operationSolver.operation.OperationStatus
import com.example.demo.services.operationSolver.operation.OperationStatus.DONE
import com.example.demo.services.operationSolver.operation.OperationStatus.REJECTED
import com.example.demo.view.MainView
import com.example.demo.view.dynamicViewer.dataBlockProvider.DataBlockProvider
import java.io.File
import tornadofx.App
import tornadofx.FileChooserMode.Save
import tornadofx.chooseFile
import tornadofx.find
import tornadofx.observableListOf

class ExtremeCalcApp : App(MainView::class) {
    val operationSolver = find<OperationSolver>()
    val fileConverter = find<FileConverter>()
    val historyService = find<HistoryService<ExtremeStep>>()
    val mainView: MainView by inject()
    val operationTypes: Array<ExtremeOperationType>
    lateinit var currentStep: ExtremeStep

    init {
        println("init app")
        operationTypes = arrayOf(
                ExtremeOperationType(PlusOperation::class, "plus", "A+x"),
                ExtremeOperationType(MinusOperation::class, "minus", "A-x"),
                ExtremeOperationType(LogarithmOperation::class, "logarithm", "Log(A) X"),
                ExtremeOperationType(RaiseOperation::class, "raise", "A^x"),
                ExtremeOperationType(MultipleOperation::class, "multiple", "A*x"),
                ExtremeOperationType(DivideOperation::class, "divide", "A/x"),
                ExtremeOperationType(SqrtOperation::class, "square", "A^(1/2)"),
                ExtremeOperationType(FactorialOperation::class, "factorial", "A!"),
                ExtremeOperationType(MedianOperation::class, "mediana", "{<A>}"),
                ExtremeOperationType(AverageOperation::class, "average", "<A>"),
                ExtremeOperationType(StandardDeviationOperation::class, "st deviation", "S(A)")
        )
        doNextStep()
    }

    fun newStepFromExisting(step: ExtremeStep) {
        doNextStep()
        mainView.bindInitView(currentStep.initOperand)
        currentStep.initOperand.writeFromFile(step.initOperand.file)
        updateStatus()
    }

    fun importData() {
        chooseFile(filters = arrayOf(fileConverter.extensionFilter)).firstOrNull()?.let { inFile ->
            if (inFile.extension != APP_FILE_EXTENSION)
                newUniqueAppFile().let {
                    fileConverter.doConverting(inFile, it)
                    currentStep.initOperand.writeFromFile(it)
                }
            else
                currentStep.initOperand.writeFromFile(inFile)
        }
    }

    fun addNewValue(newVal: String) {
        newVal.toFloatOrNull()?.let { value ->
            currentStep.initOperand.use { it.append(value) }
        }
    }

    fun clearInitOperand() {
        currentStep.initOperand.clear()
    }

    fun exportData(currentResultProvider: DataBlockProvider<String>) {
        (currentResultProvider as ExtremeFileOperand) // just i can
        chooseFile(filters = arrayOf(fileConverter.extensionFilter), mode = Save).firstOrNull()?.let {
            if (it.extension == APP_FILE_EXTENSION)
                currentResultProvider.file.copyTo(it, true)
            else
                fileConverter.doConverting(currentResultProvider.file, it)
        }
    }

    fun goToNextStep() =
            historyService.getByNumber(currentStep.number + 1)?.let { nextStep ->
                chooseStep(nextStep)
            }

    fun goToPreviousStep() {
        historyService.getByNumber(currentStep.number - 1)?.let { chooseStep(it) }
    }

    fun chooseStep(step: ExtremeStep) {
        currentStep = step
        mainView.bindInitView(currentStep.initOperand)
        mainView.bindResultView(currentStep.resultOperand)
        updateUserInput()
        updateStatus()
    }

    fun getHistory() = historyService.getHistory()

    fun solve(type: ExtremeOperationType, arg: String) {
        val init = currentStep.initOperand
        val result = currentStep.resultOperand
        if (!init.isEmpty())
            try {
                mainView.bindResultView(currentStep.resultOperand)
                operationSolver.solveOperation(
                        type.operationClass,
                        init,
                        type.parseArg(arg).toFloatOrNull(),
                        result) { progress, status ->
                    if (status == DONE || status == REJECTED) {
                        init.close()
                        result.close()
                    }
                    mainView.onProgress(progress)
                }
                commitStep(type, arg)
                newStepFromExisting(currentStep)
            } catch (ex: IllegalArgumentException) {
                println("bad input")
            }
    }

    fun getOperations() = observableListOf(*operationTypes)

    private fun updateUserInput() {
        mainView.expressionText.text = currentStep.userInput
    }

    private fun updateStatus() {
        mainView.updateStatus(currentStep.toString())
    }

    private fun commitStep(type: ExtremeOperationType, arg: String) {
        historyService.commitStep(
                currentStep.apply {
                    operationType = type
                    userInput = arg
                }
        )
    }

    private fun doNextStep() {
        currentStep = ExtremeStep(historyService.getNextStepNumber())
        ExtremeOperationType.getDefault().run { commitStep(this, placeholder) }
    }

    companion object {
        var count = 0
        fun uniqueAppFile() = File("${count++}.$APP_FILE_EXTENSION").apply { deleteOnExit() }
        fun newUniqueAppFile() = uniqueAppFile().apply {
            if (exists()) writer().flush()
            else createNewFile()
        }

        const val APP_FILE_EXTENSION = "calc"
    }
}
