package com.example.demo.view

import com.example.demo.ExtremeCalcApp
import com.example.demo.api.ExtremeStep
import tornadofx.Fragment
import tornadofx.listview
import tornadofx.observableListOf
import tornadofx.toObservable

class HistoryView : Fragment() {
    private val calcApp = this.app as ExtremeCalcApp //just I can B\
    override val root = listview(observableListOf<ExtremeStep>())

    init {
        root.items = calcApp.getHistory().toObservable()
        root.selectionModel.selectedItemProperty().addListener { observable, _, _ ->
            calcApp.chooseStep(observable.value)
            close()
        }
        //  root.cellFactory = Callback { param -> ListCell() }
    }

    fun toCellString(step: ExtremeStep) = "${step.number}  |  ${step.userInput}"
}