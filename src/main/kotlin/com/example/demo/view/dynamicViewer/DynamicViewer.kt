package com.example.demo.view.dynamicViewer

import com.example.demo.view.dynamicViewer.dataBlockProvider.DataBlockProvider
import javafx.collections.ObservableListBase
import javafx.scene.control.ListView

class DynamicViewer : ListView<String>() {
    var dataProvider: DataBlockProvider<String>? = null
        set(value) {
            items = null // todo rework in some more idiomatic way
            items = value?.let { DynamicObservableList(it) }
            field = value
        }

    class DynamicObservableList(var blockProvider: DataBlockProvider<String>) : ObservableListBase<String>() {
        override val size: Int
            get() = blockProvider.getBlockCount()

        init {
            blockProvider.onUpdate = { // todo rework in some more idiomatic way
                beginChange()
                nextUpdate(size)
                endChange()
            }
        }

        override fun get(index: Int): String = blockProvider.getBlock(index)
    }
}