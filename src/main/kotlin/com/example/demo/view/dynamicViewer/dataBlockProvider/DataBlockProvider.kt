package com.example.demo.view.dynamicViewer.dataBlockProvider

interface DataBlockProvider<T> {
    val blockSize: Int
    var onUpdate: () -> Unit
    fun getBlock(i: Int): T
    fun getBlockCount(): Int
}