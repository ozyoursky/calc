package com.example.demo.view

import com.example.demo.ExtremeCalcApp
import com.example.demo.api.ExtremeOperationType
import com.example.demo.view.dynamicViewer.DynamicViewer
import com.example.demo.view.dynamicViewer.dataBlockProvider.DataBlockProvider
import javafx.event.ActionEvent
import javafx.scene.control.ComboBox
import javafx.scene.control.Label
import javafx.scene.control.ProgressBar
import javafx.scene.control.TextField
import javafx.scene.layout.AnchorPane
import tornadofx.FileChooserMode
import tornadofx.View
import tornadofx.chooseFile

class MainView : View("Hello") {
    override val root: AnchorPane by fxml("/fxml/MainView.fxml")
    private val newValueField: TextField by fxid()
    private val initialViewer: DynamicViewer by fxid()
    val status: Label by fxid()
    private val operationsBox: ComboBox<ExtremeOperationType> by fxid()
    val expressionText: TextField by fxid()
    private val resultViewer: DynamicViewer by fxid()
    private val progressBar: ProgressBar by fxid()
    private val calcApp = this.app as ExtremeCalcApp //just I can B\

    init {
        setOperations()
        bindInitView(calcApp.currentStep.initOperand)
        bindResultView(calcApp.currentStep.resultOperand)
        updateStatus(calcApp.currentStep.toString())
        currentStage?.isAlwaysOnTop = true
    }

    fun solve(event: ActionEvent) {
        calcApp.solve(operationsBox.value, expressionText.text)
    }

    fun addNewValue() {
        calcApp.addNewValue(newValueField.characters.toString())
    }

    fun clearInput() {
        calcApp.clearInitOperand()
    }

    fun repeat() {
        calcApp.goToNextStep()
    }

    fun undo() {
        calcApp.goToPreviousStep()
    }

    fun import() {
        calcApp.importData()
    }

    fun export() {
        resultViewer.dataProvider?.let { calcApp.exportData(it) }
    }

    fun showHistory() {
        openInternalWindow<HistoryView>()
    }

    fun onSelectOperation() {
        expressionText.text = operationsBox.value.placeholder
    }

    fun onProgress(process: Double) {
        progressBar.progress = process / 100
    }

    fun bindInitView(blockProvider: DataBlockProvider<String>) {
        initialViewer.dataProvider = blockProvider
    }

    fun bindResultView(blockProvider: DataBlockProvider<String>) {
        resultViewer.dataProvider = blockProvider
    }

    fun updateStatus(newStat: String) {
        status.text = newStat
    }

    private fun setOperations() {
        operationsBox.apply {
            items = calcApp.getOperations()
            value = items[0]
            onSelectOperation()
        }
    }
}