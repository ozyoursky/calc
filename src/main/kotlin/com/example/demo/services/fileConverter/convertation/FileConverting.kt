package com.example.demo.services.fileConverter.convertation

import java.io.File
import java.io.InputStream
import java.io.OutputStream

abstract class FileConverting: Converting {
    abstract fun algorithm(inFile: File, outFile: File)
    override fun convert(inFile: File, outFile: File) {
        //some preparing stuff
        algorithm(inFile, outFile)
    }
}