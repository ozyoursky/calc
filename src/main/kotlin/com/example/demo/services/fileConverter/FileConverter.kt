package com.example.demo.services.fileConverter

import com.example.demo.ExtremeCalcApp.Companion.APP_FILE_EXTENSION
import com.example.demo.services.fileConverter.convertation.CalcToTxtConverting
import com.example.demo.services.fileConverter.convertation.Converting
import com.example.demo.services.fileConverter.convertation.TxtToCalcConverting
import java.io.File
import javafx.stage.FileChooser.ExtensionFilter
import tornadofx.Component

class FileConverter : Component() {
    val extensionFilter = ExtensionFilter("supported extensions", "*.$TXT_EXTENSION", "*.$APP_FILE_EXTENSION")
    private val converters = mapOf<Pair<String, String>, Converting>(
            (TXT_EXTENSION to APP_FILE_EXTENSION) to TxtToCalcConverting(),
            (APP_FILE_EXTENSION to TXT_EXTENSION) to CalcToTxtConverting()
    )

    fun doConverting(inFile: File, outFile: File) {
        val key = inFile.extension to outFile.extension
        converters[key]?.convert(inFile, outFile)
    }

    companion object {
        const val TXT_EXTENSION = "txt"
    }
}