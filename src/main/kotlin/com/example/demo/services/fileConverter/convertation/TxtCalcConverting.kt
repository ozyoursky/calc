package com.example.demo.services.fileConverter.convertation

import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.File

class CalcToTxtConverting : FileConverting() {
    override fun algorithm(calcFile: File, txtFile: File) {
        val flen = calcFile.length().toInt() / Float.SIZE_BYTES
        var count = flen
        val blockSize = 10
        val buffSize = 1_000_000
        val writer = txtFile.bufferedWriter(bufferSize = buffSize)
        DataInputStream(calcFile.inputStream().buffered(buffSize)).use { istr ->
            while (count > 0) {
                for (i in 1..blockSize) {
                    if (count-- > 0) {
                        writer.write(istr.readFloat().toString())
                        writer.write(" ")
                    }
                }
                writer.write("\n")
                if (count % buffSize == 0)
                    println("========== progress ${100 - (count * 100 / flen)}% ========")
            }
            writer.close()
        }
    }
}

class TxtToCalcConverting : FileConverting() {
    override fun algorithm(txtFile: File, calcFile: File) {
        val bis = txtFile.inputStream().buffered()
        DataOutputStream(calcFile.outputStream().buffered()).use { ostr ->
            val sb = StringBuilder()
            do {
                val s = bis.read()
                if (s != 32 && s != 10 && s != -1) // space, eol, eof
                    sb.append(s.toChar())
                else {
                    // println(sb)
                    sb.toString().toFloatOrNull()?.let { ostr.writeFloat(it) }
                    sb.clear()
                }
            } while (s != -1)
        }
    }

}