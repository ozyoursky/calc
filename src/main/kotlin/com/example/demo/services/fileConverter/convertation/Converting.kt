package com.example.demo.services.fileConverter.convertation

import java.io.File

interface Converting {
    fun convert(inFile: File, outFile: File)
}