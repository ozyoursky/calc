package com.example.demo.services.operationSolver.operation.operand

import java.io.Closeable
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.File
import java.io.FileOutputStream

open class SimpleFileOperand(
        var file: File,
        val inBuffer: Int = IN_BUFFER_SIZE,
        val outBuffer: Int = OUT_BUFFER_SIZE
) : ArrayOperand, Closeable {
    override var length = file.length() / Float.SIZE_BYTES
    private var oStr: DataOutputStream? = null
    private var iStr: DataInputStream? = null
    var readed = 0L

    override operator fun iterator() = object : Iterator<Float> {
        override fun next(): Float {
            if (iStr == null || !hasNext()) {
                iStr?.close()
                iStr = newInputStream().also { readed = 0 } // todo: make smarter
            }
            readed++
            return iStr?.readFloat()!!
        }
        // ~80s for 250_000_000 values without io-operations by one thread
        // ~100s for 250_000_000 values with i-operations by one thread and updating view
        // ~133s for 250_000_000 values with o-operations by one thread
        // ~145s for 250_000_000 values with io operations by one thread
        override fun hasNext() = readed < length
    }

    protected open fun newOutStream() =
            DataOutputStream(FileOutputStream(file, true).buffered(outBuffer))

    protected open fun newInputStream() =
            DataInputStream(file.inputStream().buffered(inBuffer))

    override fun close() {
        oStr?.close()
        iStr?.close()
        oStr = null
        iStr = null
    }

    override fun append(n: Float) {
        if (oStr == null)
            oStr = newOutStream()
        oStr?.writeFloat(n)
        length++
    }

    override fun clear() {
        file.writer().use { it.flush() }
        length = 0
    }

    companion object {
        const val IN_BUFFER_SIZE = 1_000_000
        const val OUT_BUFFER_SIZE = 1_000_000
    }
}