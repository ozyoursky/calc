package com.example.demo.services.operationSolver.operation

enum class OperationStatus {
        DONE, PENDING, REJECTED
}