package com.example.demo.services.operationSolver.operation.operations

import com.example.demo.services.operationSolver.operation.Operation
import kotlin.math.log
import kotlin.math.pow

abstract class VectorOperation : Operation() {
    fun doForEach(action: (next: Float) -> Float) {
        operand.iterator().run {
            while (hasNext()) {
                processed++
                doProgress(action(next()))
            }
        }
    }
}

abstract class OneArgumentOperation : VectorOperation() {
    private val exception = IllegalArgumentException("argument must not be null")

    abstract fun action(val1: Float, val2: Float): Float

    override fun algorithm() {
        argument?.let { arg -> doForEach { next -> action(next, arg) } } ?: throw exception
    }

    override fun validate() {
        if (argument == null) throw exception
    }
}

abstract class NoArgumentOperation : VectorOperation() {
    abstract fun action(val1: Float): Float

    override fun algorithm() {
        doForEach { next -> action(next) }
    }
}

class PlusOperation : OneArgumentOperation() {
    override fun action(val1: Float, val2: Float) = val1 + val2
}

class MinusOperation : OneArgumentOperation() {
    override fun action(val1: Float, val2: Float) = val1 - val2
}

class LogarithmOperation : OneArgumentOperation() {
    override fun action(val1: Float, val2: Float) = log(val1, val2)
}

open class RaiseOperation : OneArgumentOperation() {
    override fun action(val1: Float, val2: Float) = val1.pow(val2)
}

class MultipleOperation : OneArgumentOperation() {
    override fun action(val1: Float, val2: Float) = val1 * val2
}

class DivideOperation : OneArgumentOperation() {
    override fun action(val1: Float, val2: Float) = val1 / val2
}

class FactorialOperation : NoArgumentOperation() {
    override fun action(val1: Float): Float {
        var k = 1
        for (i in 1..val1.toInt()) k *= i
        return k.toFloat()
    }
}

class SqrtOperation : NoArgumentOperation() {
    override fun action(val1: Float) = val1.pow(0.5f)
}