package com.example.demo.services.operationSolver.operation.operand

interface ArrayOperand: Iterable<Float>{
    val length: Long
    fun append(n: Float)
    fun clear()
}