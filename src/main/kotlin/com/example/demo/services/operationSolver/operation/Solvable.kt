package com.example.demo.services.operationSolver.operation

interface Solvable {
    fun solve()
}