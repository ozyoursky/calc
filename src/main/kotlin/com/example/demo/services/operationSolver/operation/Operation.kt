package com.example.demo.services.operationSolver.operation

import com.example.demo.services.operationSolver.operation.OperationStatus.DONE
import com.example.demo.services.operationSolver.operation.OperationStatus.PENDING
import com.example.demo.services.operationSolver.operation.OperationStatus.REJECTED
import com.example.demo.services.operationSolver.operation.operand.ArrayOperand

abstract class Operation : Solvable {
    lateinit var operand: ArrayOperand
    lateinit var onProgress: (nextVal: Number?, progress: Double, status: OperationStatus) -> Unit
    var argument: Float? = null
    var processed = 0.0

    abstract fun algorithm()

    open fun validate() {}

    override fun solve() {
        try {
            algorithm()
        } catch (e: Exception) {
            onProgress(null, processed.getPercent(), REJECTED)
            throw e
        }
        onProgress(null, processed.getPercent(), DONE)
    }

    private fun Double.getPercent() = this * HUNDRED_PERCENT / operand.length

    fun doProgress(nextVal: Number?) = onProgress(nextVal, processed.getPercent(), PENDING)

    companion object {
        const val HUNDRED_PERCENT = 100.0
    }
}