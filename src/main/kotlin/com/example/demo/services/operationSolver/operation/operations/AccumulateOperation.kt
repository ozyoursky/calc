package com.example.demo.services.operationSolver.operation.operations

import com.example.demo.services.operationSolver.operation.Operation
import com.example.demo.services.operationSolver.operation.operand.SimpleFileOperand
import java.io.File
import java.util.UUID.randomUUID
import kotlin.math.pow

abstract class AccumulateOperation : Operation() {
    //some stuff
}

class AverageOperation : AccumulateOperation() {
    override fun algorithm() {
        var sum = 0f
        val iterator = operand.iterator()
        while (iterator.hasNext()) {
            sum += iterator.next()
            processed++
            doProgress(null)
        }
        doProgress(sum / processed)
    }
}

class MedianOperation : AccumulateOperation() {
    data class TreePair(var head: Float, var iterator: Iterator<Float>)

    override fun algorithm() {
        val bufferSize = 1_000_000
        val filesList = mutableListOf<SimpleFileOperand>()
        val buffer = FloatArray(bufferSize)
        val tree = mutableListOf<TreePair>()
        val iterator = operand.iterator()
        try {
            while (iterator.hasNext()) {
                val buff = fillBuffer(buffer, iterator)
                buff.sort()
                filesList.add(
                        createTempOperand().apply { writeBuffer(buff) }
                )
                processed += bufferSize / 2
                doProgress(null)
            }

            filesList.forEach {
                with(it.iterator()) {
                    tree.add(TreePair(next(), this))
                }
            }
            var prev = 0f
            processed = operand.length / 2.0

            for (i in 0 until operand.length / 2) {
                prev = tree.nextMin()
                processed++
                doProgress(null)
            }

            if (operand.length % 2 == 0L)
                doProgress((prev + tree.nextMin()) / 2)
            else
                doProgress(tree.nextMin())
        } finally {
            filesList.forEach { it.close() }
        }
    }

    private fun fillBuffer(arr: FloatArray, iter: Iterator<Float>): FloatArray {
        for (i in arr.indices) {
            if (!iter.hasNext()) {
                return arr.dropLast(arr.size - i).toFloatArray()
            }
            arr[i] = iter.next()
        }
        return arr
    }

    private fun MutableList<TreePair>.nextMin(): Float {
        minByOrNull { it.head }?.let {
            val min = it.head
            if (it.iterator.hasNext())
                it.head = it.iterator.next()
            else {
                this.remove(it)
            }
            return min
        } ?: return this[0].head
    }

    private fun SimpleFileOperand.writeBuffer(buff: FloatArray) {
        this.use {
            for (v in buff.iterator())
                it.append(v)
        }
    }
}

class StandardDeviationOperation : AccumulateOperation() {
    override fun algorithm() {
        val average = solveAverage()
        var sqSum = 0.0
        val iterator = operand.iterator()
        do {
            sqSum += (iterator.next().toDouble() - average).pow(2.0)
            processed += 0.5
            doProgress(null)
        } while (iterator.hasNext())
        doProgress(
                (sqSum / (operand.length - 1)).pow(0.5)
        )
    }

    private fun solveAverage(): Float {
        var average = 0f
        val mainOperation = this
        AverageOperation().apply {
            operand = mainOperation.operand
            onProgress = { nextVal, _, _ ->
                if (nextVal != null)
                    average = nextVal.toFloat()
                else
                    mainOperation.processed += 0.5
                mainOperation.doProgress(null)
            }
        }.solve()
        return average
    }
}

fun randomString() = randomUUID().toString()
fun createTempOperand(inBuff: Int = 8_000, outBuff: Int = 8_000) =
        SimpleFileOperand(
                File("${randomString()}.tmp").apply { deleteOnExit() },
                inBuff,
                outBuff
        )