package com.example.demo.services.operationSolver

import com.example.demo.services.operationSolver.operation.Operation
import com.example.demo.services.operationSolver.operation.OperationStatus
import com.example.demo.services.operationSolver.operation.operand.ArrayOperand
import kotlin.concurrent.thread
import kotlin.reflect.KClass
import kotlin.reflect.full.primaryConstructor
import tornadofx.Component

class OperationSolver : Component() {
    fun solveOperation(
            opClass: KClass<out Operation>,
            initOperand: ArrayOperand,
            argument: Float?,
            resOperand: ArrayOperand,
            onProgress: (progress: Double, status: OperationStatus) -> Unit = { _, _ -> }
    ) {
        val operation = opClass.buildOperation(initOperand, argument) { nextVal, progress, status ->
            nextVal?.let { v ->
                resOperand.append(v.toFloat())
            }
            onProgress(progress, status)
        }
        thread { //todo rework with Task
            resOperand.clear()
            operation?.solve()
        }
    }

    private fun KClass<out Operation>.buildOperation(
            op: ArrayOperand,
            arg: Float?,
            progress: (nextVal: Number?, progress: Double, status: OperationStatus) -> Unit
    ) = primaryConstructor?.call()
            ?.apply {
                operand = op
                arg?.let { argument = it }
                onProgress = progress
                validate()
            }
}

