package com.example.demo.services.historyService

interface HistoryStep {
    val number: Int
}