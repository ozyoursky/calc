package com.example.demo.services.historyService

import tornadofx.Component

class HistoryService<T> : Component() where T : HistoryStep {
    private var historyList: MutableList<T> = mutableListOf()
    fun commitStep(step: T) {
        if (historyList.contains(step))
            for (i in 0 until historyList.lastIndex - step.number) {
                historyList.removeLast()
            }
        else
            historyList.add(step)
    }

    fun getHistory(): List<T> {
        return historyList
    }

    fun isLast(step: T) = step.number == historyList.lastIndex
    fun getNextStepNumber() = historyList.size
    fun getByNumber(n: Int): T? = historyList.find { step -> step.number == n }
}